#define _CRT_SECURE_NO_WARNINGS
#include "comb.h"

int comparator(const void* request1, const void* request2) {
    Request* request_a = (Request*)request1;
    Request* request_b = (Request*)request2;

    return request_a->time_taken - request_b->time_taken;
}

Request* create_request_list(FILE* input) {

    fscanf(input, "%d", &req_amount);
    Request* requests = (Request*)calloc(req_amount, sizeof(Request));

    Time time_taken, time_released;

    for (int counter = 0; counter < req_amount; counter++) {
        fscanf(input, "%d %hd:%hd %hd:%hd", &(requests[counter].mem_vol), &(time_taken.hours), &(time_taken.minutes), &(time_released.hours), &(time_released.minutes));
        requests[counter].time_taken = time_taken.hours * 60 + time_taken.minutes;
        requests[counter].time_released = time_released.hours * 60 + time_released.minutes;
    }

    return requests;
}

void calc_min_mem(Request* requests, int req_amount) {

    qsort(requests, req_amount, sizeof(Request), comparator);

    int last_minute = 0;
    for (int i = 0; i < req_amount; i++) {
        if (last_minute < requests[i].time_released) {
            last_minute = requests[i].time_released;
        }
    }

    int request_counter = 0;

    for (int current_minute = requests[0].time_taken; current_minute < last_minute; current_minute++) {
        for (request_counter = 0; request_counter < req_amount; request_counter++) {
            if (requests[request_counter].time_taken == current_minute) {
                mem_taken += requests[request_counter].mem_vol;
                if (min_mem_required < mem_taken) {
                    min_mem_required = mem_taken;
                }
            }
            if (requests[request_counter].time_released == current_minute) {
                mem_taken -= requests[request_counter].mem_vol;
            }
        }
    }
}


int maxMemRequest(Request* requests, int to, int indexRequest)
{
    int temp, maxMemory = 0;
    for (int i = indexRequest + 1; i < req_amount && requests[i].time_taken < to && maxTo < to; i++)
    {
        if (requests[i].time_released > maxTo)
        {
            temp = maxMemRequest(requests, (requests[i].time_released < to) ? requests[i].time_released : to, i);
            if (maxMemory < temp)
            {
                maxMemory = temp;
            }
            maxTo = (requests[i].time_released < to) ? requests[i].time_released : to;
        }
    }
    return requests[indexRequest].mem_vol + maxMemory;
}

int minMemForAllRequest(Request* requests)
{
    qsort(requests, req_amount, sizeof(Request), comparator);
    int temp, min = 0;

    for (int i = 0; i < req_amount; i++)
    {
        if (requests[i].time_released > maxTo)
        {
            temp = maxMemRequest(requests, requests[i].time_released, i);
            if (min < temp)
            {
                min = temp;
            }

            maxTo = requests[i].time_released;
        }
    }
    return min;
}
