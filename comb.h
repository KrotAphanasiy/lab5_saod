#ifndef COMB_H
#define COMB_H
#include <stdio.h>
#include <stdlib.h>

extern int req_amount;
extern int mem_taken;
extern int min_mem_required;
extern int maxTo;

struct Time {
    unsigned short int hours;
    unsigned short int minutes;
};

struct Request {
    int mem_vol = 0;
    int time_taken = 0;
    int time_released = 0;
};

Request* create_request_list(FILE* input);
void calc_min_mem(Request* requests, int req_amount);
int minMemForAllRequest(Request* requests);

#endif
